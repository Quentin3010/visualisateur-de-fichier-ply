# Visualiseur de fichier PLY

Quentin BERNARD & Noé Delcroix & Constant VENNIN & Robin LEFEBVRE

## Description du projet

Le “Visualisaeur de fichier PLY" est un logiciel Java développé avec JavaFX qui offre une solution pratique et conviviale pour visualiser des modèles 3D au format PLY. Il offre aux utilisateurs la possibilité d'explorer et d'interagir avec des modèles 3D de manière intuitive et fluide.

Avec ce visualiseur, vous pouvez facilement charger et afficher des modèles PLY, qu'il s'agisse de modèles créés par vous-même ou téléchargés depuis des sources externes. L'interface utilisateur élégante et bien conçue facilite la navigation dans la liste de modèles disponibles.

Une fois que vous avez sélectionné un modèle, vous pouvez profiter de nombreuses fonctionnalités pour l'explorer sous tous les angles. Le logiciel offre des options de rotation, de translation et d'homothétie pour manipuler le modèle selon vos préférences. Vous pouvez également choisir entre différentes vues, telles que la vue de face, la vue de haut et la vue de côté, pour obtenir une perspective complète du modèle.

L'application est dotée d'une interface réactive et rapide, ce qui permet une expérience utilisateur agréable. De plus, elle prend en charge le rendu en temps réel des modèles, ce qui vous permet de voir les changements instantanément lorsque vous effectuez des manipulations sur le modèle.

Vidéo de présentation : lien

## Fonctionnalités

- Affichage de la liste détaillée des modèles
- Sélection du modèle à visualiser dans la liste
- Chargement et visualisation du modèle sélectionné
- Gestion des erreurs de format dans les fichiers
- Affichage simultané de trois vues du modèle (de face, de haut, de côté)
- Rotation du modèle
- Translation du modèle
- Homothétie du modèle
- Tests pour les classes de calcul mathématique
- Affichage des faces seulement ou des segments seulement
- Affichage avancé de la bibliothèque de modèles
- Recherche dans la bibliothèque de modèles
- Édition des informations sur un modèle
- Modèle centré dans la vue
- Éclairage du modèle
- Lissage du modèle
- Ombre portée pour le modèle
- Vue en tranches du modèle
- Contrôleur horloge
- Interface évoluée avec presets pour les canvas
- Renommage des fichiers PLY depuis l'application
- Rendu seulement des faces visibles
- Customisation des couleurs (faces, fond, arêtes)

## Mon rôle

Ce projet a été réalisé en équipe, et ma mission consistait à développer la classe Model et plusieurs classes associées, ainsi que la fonction de lecture des fichiers PLY. Nous avons également mis en place des exceptions spécifiques pour gérer les éventuelles erreurs liées à la lecture des fichiers PLY. En plus de cela, j'ai contribué à la mise en place de tests pour assurer la robustesse et la qualité du code. Une autre fonctionnalité clé que j'ai développée était la lecture des couleurs associées aux modèles. En outre, j'ai participé à la conception et à l'implémentation du contrôleur horloge, qui permet de gérer les animations et les mouvements temporels dans l'application. Grâce à ces efforts combinés, nous avons pu créer un logiciel performant et polyvalent pour la visualisation de fichiers PLY.

